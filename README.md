# Flying Bisons test app

Application created with Expo in React Native for a recruitment assignment.

## How To Use

```bash
# Clone this repository
$ git clone https://gitlab.com/ushyDev/flyingbisons-test-app.git

# Go into the repository
$ cd flyingbisons

# Install dependencies
$ yarn 

# Run Expo app for ios
$ expo start  -i
# or for android:
$ expo start  -a

```

## Folder Structure

After creation, your project should look like this:

```
flyingbisons
├─ README.md
├─ App.tsx
├─ app.json
├─ assets
├─ package.json
├─ src
│  ├─ components
│  │  ├─ AlbumItem
│  │  ├─ Button
│  │  ├─ Comment
│  │  ├─ Input
│  │  ├─ NewsDetailHeader
│  │  └─ NewsItem
│  ├─ containers
│  │  ├─ Screen
│  │  └─ ScrollScreen
│  ├─ context
│  ├─ interfaces
│  ├─ navigation
│  ├─ screens
│  │  ├─ Albums
│  │  ├─ DetailNews
│  │  ├─ Loading
│  │  └─ News
│  ├─ services
│  │  ├─ albumsApi
│  │  ├─ commentsApi
│  │  └─ newsApi
│  ├─ styles
│  │  ├─ colors.ts
│  │  └─ typography.ts
│  └─ utils
│     └─ text.ts
└─ tsconfig.json

```

## Libraries I used
#### Navigation
For the main navigation (newsy, albums) I used the react-navigation-tabs library, with createMaterialTopTabNavigator I can create a TabNavigator in which there is a swipe option to move between tabs, users who have large phones will appreciate this.

To navigate between the news screen and the news details, I used a react-native-shared-element to create a news item transition effect.

#### Expo Libraries

I used expo-font and @expo-google-fonts/roboto to load the fonts before going to the main app screen. I used Roboto font as a replacement for Gilroy.

To check the user's connection to the internet I used expo-network.

#### Other

For ScrollView with KeyboardAvoidingView I used react-native-keyboard-aware-scroll-view.

I used Context Api to store state in the application


### What I would improve:

I would write tests for the application using the library Jest.
