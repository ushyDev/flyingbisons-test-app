module.exports = function (api) {
  api.cache(true);
  return {
    presets: ["babel-preset-expo"],
    plugins: [
      [
        "module-resolver",
        {
          alias: {
            assets: "./assets",
            components: "./src/components",
            containers: "./src/containers",
            screens: "./src/screens",
            styles: "./src/styles",
            services: "./src/services",
            interfaces: "./src/interfaces",
            navigation: "./src/navigation",
            context: "./src/context",
            utils: "./src/utils",
          },
        },
      ],
    ],
  };
};
