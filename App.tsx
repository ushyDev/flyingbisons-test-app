import React, { useEffect, useState } from "react";
import MainNavigator from "./src/navigation/MainNavigator";
import NavigationServices from "./src/navigation/NavigationServices";
import {
  useFonts,
  Roboto_400Regular,
  Roboto_500Medium,
  Roboto_700Bold,
  Roboto_900Black,
} from "@expo-google-fonts/roboto";
import Loading from "screens/Loading";
import { Alert, LogBox } from "react-native";
import * as Network from "expo-network";
import AppProvider from "./src/context/AppProvider";

export default function App() {
  let [fontsLoaded] = useFonts({
    Roboto_400Regular,
    Roboto_500Medium,
    Roboto_700Bold,
    Roboto_900Black,
  });
  const [isInternet, setIsInternet] = useState<boolean | undefined>(false);
  useEffect(() => {
    async function networkCheck() {
      try {
        const networkState = await Network.getNetworkStateAsync();
        setIsInternet(networkState.isConnected);
      } catch (e: any) {
        Alert.alert("useIsInternetReachable error:", e.message);
      }
    }
    networkCheck();
  }, []);


  if (!fontsLoaded || !isInternet) {
    return <Loading isInternet={isInternet} />;
  } else {
    return (
      <AppProvider>
        <MainNavigator
          ref={(navigatorRef) => {
            NavigationServices.setTopLevelNavigator(navigatorRef);
          }}
        />
      </AppProvider>
    );
  }
}
