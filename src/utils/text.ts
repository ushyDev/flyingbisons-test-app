export const capitalizeFirstLetter = (string: string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};
export const truncate = (input: string, num: number) =>
  input.length > 5 ? `${input.substring(0, num)}...` : input;
