import { Comments } from "interfaces/Comments";

export const fetchComments = async (
  id: number
): Promise<Comments[] | undefined> => {
  try {
    const response = await fetch(
      `https://jsonplaceholder.typicode.com/posts/${id}/comments`
    );
    const json = await response.json();
    return json;
  } catch (error) {
    console.error(error);
  }
};

export const sendComment = async (
  id: number,
  comment: string
): Promise<any> => {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ body: comment }),
  };
  try {
    const response = await fetch(
      `https://jsonplaceholder.typicode.com/posts/${id}/comments`,
      requestOptions
    );
    const json = await response.status;
    return json;
  } catch (error) {
    console.error(error);
  }
};
