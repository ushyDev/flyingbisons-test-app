import { Albums } from "interfaces/Albums";
import { Photos } from "interfaces/Photos";

export const fetchAlbums = async (): Promise<Albums[] | undefined> => {
  try {
    const response = await fetch(`https://jsonplaceholder.typicode.com/albums`);
    const json = await response.json();
    return json;
  } catch (error) {
    console.error(error);
  }
};

export const fetchPhotos = async (
  id: number
): Promise<Photos[] | undefined> => {
  try {
    const response = await fetch(
      `https://jsonplaceholder.typicode.com/albums/${id}/photos`
    );
    const json = await response.json();
    return json;
  } catch (error) {
    console.error(error);
  }
};
