import { Posts } from "interfaces/Posts";

export const fetchPosts = async (): Promise<Posts[] | undefined> => {
  try {
    const response = await fetch(`https://jsonplaceholder.typicode.com/posts`);
    const json = await response.json();
    return json;
  } catch (error) {
    console.error(error);
  }
};
