export const COLORS = {
  white: "#fff",
  black: "#001524",
  bg: "#F4F6F9",
  indigo: "#466BC9",
  lightIndigo: "#ECF0FA",
  gray: "#A0ABB2",
  darkGray: "#586976",
  lightGray: "#F0F2F5",
  error: "#f06767",
  correct: "#49d408",
};
