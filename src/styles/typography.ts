import { COLORS } from "./colors";

export const TYPOGRAPHY = {
  h1: {
    fontFamily: "Roboto_900Black",
    fontSize: 16,
    color: COLORS.black,
  },
  h2: {
    fontFamily: "Roboto_700Bold",
    fontSize: 14,
    color: COLORS.black,
  },
  regular: {
    fontFamily: "Roboto_400Regular",
    fontSize: 14,
    color: COLORS.black,
  },
  medium: {
    fontFamily: "Roboto_500Medium",
    fontSize: 14,
    color: COLORS.black,
  },
};
