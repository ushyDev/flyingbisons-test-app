import React, { useMemo } from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import styles from "./styles";
import { Posts } from "interfaces/Posts";
import { capitalizeFirstLetter, truncate } from "utils/text";
import NavigationServices from "navigation/NavigationServices";
import { SharedElement } from "react-navigation-shared-element/build/v4";

const NewsItem = ({ item }: { item: Posts }) => {
  const handlePressItem = () => {
    NavigationServices.navigate("detailNews", { item: item });
  };
  const capitalizeTitle = useMemo(() => capitalizeFirstLetter(item.title), []);
  const truncateBody = useMemo(
    () => truncate(capitalizeFirstLetter(item.body), 100),
    []
  );

  return (
    <SharedElement id={`item.${item.id}`}>
      <TouchableOpacity onPress={handlePressItem} style={styles.container}>
        <View style={styles.contentContainer}>
          <Text style={styles.contentTitle}>{capitalizeTitle}</Text>
          <Text style={styles.contentBody}>{truncateBody}</Text>
        </View>
        <View style={styles.iconContainer}>
          <Image
            width={24}
            height={24}
            source={require("../../../assets/Arrow-Right.png")}
          />
        </View>
      </TouchableOpacity>
    </SharedElement>
  );
};

export default NewsItem;
