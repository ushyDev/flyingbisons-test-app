import { StyleSheet } from "react-native";
import { COLORS } from "styles/colors";
import { TYPOGRAPHY } from "styles/typography";

const styles = StyleSheet.create({
  container: {
    width: "100%",
    backgroundColor: COLORS.white,
    minHeight: 135,
    marginBottom: 8,
    borderRadius: 8,
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 24,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,

    elevation: 1,
  },
  contentContainer: {
    width: "80%",
  },
  contentContainerDetail: {
    width: "100%",
  },
  iconContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
  contentTitle: {
    ...TYPOGRAPHY.h2,
    marginBottom: 4,
  },
  contentTitleDetail: {
    ...TYPOGRAPHY.h2,
    fontSize: 20,
  },
  contentBody: {
    ...TYPOGRAPHY.h2,
    color: COLORS.gray,
  },
});

export default styles;
