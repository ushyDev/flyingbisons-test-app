import { StyleSheet } from "react-native";
import { COLORS } from "styles/colors";
import { TYPOGRAPHY } from "styles/typography";

const styles = StyleSheet.create({
  inputContainer: {
    ...TYPOGRAPHY.medium,
    backgroundColor: COLORS.white,
    borderRadius: 8,
    minHeight: 125,
    paddingHorizontal: 16,
    paddingTop: 20,
    color: COLORS.black,
    borderWidth: 1,
  },
});

export default styles;
