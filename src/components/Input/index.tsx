import React from "react";
import { TextInput } from "react-native";
import styles from "./styles";
import { COLORS } from "styles/colors";

type InputProps = {
  onChangeText: (text: string) => void;
  placeholder?: string;
  value: string;
  style?: any;
  error?: boolean;
};

const Input = (props: InputProps) => {
  return (
    <TextInput
      style={[
        styles.inputContainer,
        props.style,
        { borderColor: props.error ? COLORS.error : COLORS.white },
      ]}
      onChangeText={props.onChangeText}
      value={props.value}
      placeholder={props.placeholder}
      multiline={true}
      placeholderTextColor={COLORS.darkGray}
    />
  );
};

export default Input;
