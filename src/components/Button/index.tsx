import React from "react";
import { Text, TouchableOpacity, ActivityIndicator } from "react-native";
import { COLORS } from "styles/colors";
import styles from "./styles";

type ButtonProps = {
  onPress: () => void;
  title: string;
  style?: any;
  loading?: boolean;
};

const Button = (props: ButtonProps) => {
  return (
    <TouchableOpacity
      activeOpacity={0.5}
      onPress={props.onPress}
      style={[styles.buttonContainer, props.style]}
    >
      {!props.loading ? (
        <Text style={styles.buttonTitle}>{props.title}</Text>
      ) : (
        <ActivityIndicator color={COLORS.white} />
      )}
    </TouchableOpacity>
  );
};

export default Button;
