import { StyleSheet } from "react-native";
import { COLORS } from "styles/colors";
import { TYPOGRAPHY } from "styles/typography";

const styles = StyleSheet.create({
  buttonContainer: {
    backgroundColor: COLORS.indigo,
    borderRadius: 8,
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 16,
  },
  buttonTitle: {
    ...TYPOGRAPHY.medium,
    color: COLORS.white,
  },
});

export default styles;
