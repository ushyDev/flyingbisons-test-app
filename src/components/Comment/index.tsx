import React from "react";
import { View, Text } from "react-native";
import styles from "./styles";
import { Comments } from "interfaces/Comments";

const Comment = ({ item }: { item: Comments }) => {
  return (
    <View style={styles.container}>
      <View style={styles.contentContainer}>
        <Text style={styles.contentTitle}>{item.name}</Text>
        <Text style={styles.contentBody}>{item.body}</Text>
      </View>
    </View>
  );
};

export default Comment;
