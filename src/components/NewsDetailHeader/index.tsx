import React, { useMemo } from "react";
import { View, Text } from "react-native";
import styles from "./styles";
import { Posts } from "interfaces/Posts";
import { capitalizeFirstLetter } from "utils/text";
import { SharedElement } from "react-navigation-shared-element/build/v4";

const NewsDetailHeader = ({ item }: { item: Posts }) => {
  const capitalizeTitle = useMemo(() => capitalizeFirstLetter(item.title), []);
  const capitalizeBody = useMemo(() => capitalizeFirstLetter(item.body), []);

  return (
    <SharedElement id={`item.${item.id}`}>
      <View style={styles.container}>
        <View style={styles.contentContainer}>
          <Text style={styles.contentTitle}>{capitalizeTitle}</Text>
          <Text style={styles.contentBody}>{capitalizeBody}</Text>
        </View>
      </View>
    </SharedElement>
  );
};

export default NewsDetailHeader;
