import { StyleSheet } from "react-native";
import { COLORS } from "styles/colors";
import { TYPOGRAPHY } from "styles/typography";

const styles = StyleSheet.create({
  container: {
    width: "100%",
    backgroundColor: COLORS.white,
    marginBottom: 6,
    borderRadius: 8,
    padding: 24,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,

    elevation: 1,
  },
  contentContainer: {
    width: "100%",
  },
  contentTitle: {
    ...TYPOGRAPHY.h2,
    fontSize: 20,
    marginBottom: 12,
  },
  contentBody: {
    ...TYPOGRAPHY.regular,
    lineHeight: 20,
  },
});

export default styles;
