import { StyleSheet } from "react-native";
import { COLORS } from "styles/colors";
import { TYPOGRAPHY } from "styles/typography";

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.white,
    width: "46%",
    minHeight: 140,
    marginBottom: 8,
    borderRadius: 8,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,

    elevation: 1,
  },
  insideContainer: {
    width: "100%",
    height: "100%",
    overflow: "hidden",
    borderRadius: 8,
  },
  image: {
    width: "100%",
    height: 100,
    resizeMode: "cover",
  },
  titleContainer: {
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 12,
    paddingHorizontal: 4,
  },
  title: {
    ...TYPOGRAPHY.medium,
    textAlign: "center",
  },
});

export default styles;
