import React, { useState, useEffect } from "react";
import { View, Text, Image, ActivityIndicator } from "react-native";
import { Albums } from "interfaces/Albums";
import styles from "./styles";
import { fetchPhotos } from "services/AlbumsApi";
import { Photos } from "interfaces/Photos";
import { capitalizeFirstLetter } from "utils/text";

const AlbumItem = ({ item }: { item: Albums }) => {
  const [photos, setPhotos] = useState<Photos[]>([]);
  const [loaded, setLoaded] = useState<boolean>(false);
  const setPhotosData = async () => {
    const dataToSet = await fetchPhotos(item.id);
    if (dataToSet != undefined) {
      setPhotos(dataToSet);
      setLoaded(true);
    }
  };
  useEffect(() => {
    setPhotosData();
  }, []);
  return (
    <View style={styles.container}>
      <View style={styles.insideContainer}>
        {loaded ? (
          <Image style={styles.image} source={{ uri: photos[0].url }} />
        ) : (
          <ActivityIndicator />
        )}
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{capitalizeFirstLetter(item.title)}</Text>
        </View>
      </View>
    </View>
  );
};

export default AlbumItem;
