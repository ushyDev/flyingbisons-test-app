import { createSharedElementStackNavigator } from "react-navigation-shared-element/build/v4";
import Albums from "screens/Albums";

const AlbumsNavigator = createSharedElementStackNavigator(
  {
    albums: {
      screen: Albums,
      navigationOptions: {
        title: "Albums",
      },
    },
  },
  {
    initialRouteName: "albums",
    defaultNavigationOptions: {
      headerStyle: {
        height: 88 + 20, //20 notch
      },
      headerTitleStyle: {
        fontFamily: "Roboto_900Black",
      },
    },
  }
);

export default AlbumsNavigator;
