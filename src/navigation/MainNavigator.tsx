import { Platform } from "react-native";
import { createAppContainer } from "react-navigation";
import { createMaterialTopTabNavigator } from "react-navigation-tabs";
import { COLORS } from "../styles/colors";

import NewsNavigator from "./NewsNavigation";
import AlbumsNavigator from "./AlbumNavigator";

const MainNavigator = createMaterialTopTabNavigator(
  {
    news: {
      screen: NewsNavigator,
      navigationOptions: {
        title: "News",
      },
    },
    album: {
      screen: AlbumsNavigator,
      navigationOptions: {
        title: "Albums",
      },
    },
  },
  {
    initialRouteName: "news",
    tabBarPosition: "bottom",
    lazy: true,
    tabBarOptions: {
      activeTintColor: COLORS.indigo,
      inactiveTintColor: COLORS.gray,
      showIcon: false,
      showLabel: true,
      upperCaseLabel: false,
      indicatorStyle: {
        backgroundColor: COLORS.indigo,
        height: 2,
      },
      style: {
        height: 55,
        backgroundColor: COLORS.white,
        marginBottom: Platform.OS === "ios" ? 20 : 0,
        borderTopWidth: 1,
        borderColor: COLORS.lightGray,
      },
      labelStyle: {
        fontFamily: "Roboto_900Black",
        fontSize: 14,
      },
    },
  }
);

export default createAppContainer(MainNavigator);
