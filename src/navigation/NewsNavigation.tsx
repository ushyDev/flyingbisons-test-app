import React from "react";
import { Image } from "react-native";
import { createSharedElementStackNavigator } from "react-navigation-shared-element/build/v4";
import News from "screens/News";
import DetailNews from "screens/DetailNews";

const BackButton = () => {
  return (
    <Image
      width={40}
      height={40}
      source={require("../../assets/IconLeft.png")}
      style={{ marginLeft: 24 }}
    />
  );
};

const NewsNavigator = createSharedElementStackNavigator(
  {
    news: {
      screen: News,
      navigationOptions: {
        title: "News list",
      },
    },
    detailNews: {
      screen: DetailNews,
      navigationOptions: {
        title: "News details",
        headerBackImage: () => <BackButton />,
        headerBackTitle: " ",
      },
    },
  },
  {
    initialRouteName: "news",
    defaultNavigationOptions: {
      headerStyle: {
        height: 88 + 20, //20 notch
      },
      headerTitleStyle: {
        fontFamily: "Roboto_900Black",
      },
    },

  }
);

export default NewsNavigator;
