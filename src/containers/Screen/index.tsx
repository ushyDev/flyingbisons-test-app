import React from "react";
import { View,  SafeAreaView } from "react-native";
import { COLORS } from "styles/colors";

type ScreenProps = {
  children: React.ReactNode;
  color?: string;
  style?: any;
};

const Screen = ({ children, color, style }: ScreenProps) => {
  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: color ? color : COLORS.bg,
      }}
    >
      <View
        style={[
          {
            flex: 1,
            backgroundColor: color ? color : COLORS.bg,
          },
          style,
        ]}
      >
        {children}
      </View>
    </SafeAreaView>
  );
};

export default Screen;
