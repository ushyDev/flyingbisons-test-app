import React from "react";
import { SafeAreaView } from "react-native";
import { COLORS } from "styles/colors";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

type ScrollScreenProps = {
  children: React.ReactNode;
  color?: string;
  extraScrollHeight?: number;
  style?: any;
};

const ScrollScreen = ({
  children,
  color,
  extraScrollHeight,
  style,
}: ScrollScreenProps) => {
  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: color ? color : COLORS.bg,
      }}
    >
      <KeyboardAwareScrollView
        style={[
          {
            flex: 1,
            backgroundColor: color ? color : COLORS.bg,
          },
          style,
        ]}
        extraScrollHeight={extraScrollHeight}
      >
        {children}
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

export default ScrollScreen;
