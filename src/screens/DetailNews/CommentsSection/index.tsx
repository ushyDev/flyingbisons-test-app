import React, { useState } from "react";
import { View, Text } from "react-native";
import Comment from "components/Comment";
import { Comments } from "interfaces/Comments";
import styles from "./styles";
import Input from "components/Input";
import Button from "components/Button";
import { sendComment } from "services/commentsApi";
import { COLORS } from "styles/colors";

type CommentsListProps = {
  data: Comments[];
  itemID: number;
};

const CommentsList = ({ data, itemID }: CommentsListProps) => {
  const [commentText, setCommentText] = useState<string>("");
  const [statusHTTP, setStatusHTTP] = useState<number>();
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);

  const handleButtonAdd = async () => {
    if (commentText != "") {
      setError(false);
      setLoading(true);
      const dataToSet = await sendComment(itemID, commentText);
      if (dataToSet != undefined) {
        setStatusHTTP(dataToSet);
        setLoading(false);
        setCommentText("");
      }
    } else {
      setError(true);
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.containerTitle}>Comments</Text>
      <Input
        value={commentText}
        onChangeText={(text) => setCommentText(text)}
        placeholder="Here you can add your comment..."
        style={styles.input}
        error={error}
      />
      <Button
        style={styles.button}
        loading={loading}
        title="Add"
        onPress={handleButtonAdd}
      />
      {statusHTTP && (
        <Text
          style={[
            styles.statusText,
            {
              color: statusHTTP === 201 ? COLORS.correct : COLORS.error,
            },
          ]}
        >
          Status: {statusHTTP}
        </Text>
      )}
      {data.map((el) => (
        <Comment key={el.id} item={el} />
      ))}
    </View>
  );
};

export default CommentsList;
