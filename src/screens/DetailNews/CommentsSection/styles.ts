import { StyleSheet } from "react-native";
import { COLORS } from "styles/colors";
import { TYPOGRAPHY } from "styles/typography";

const styles = StyleSheet.create({
  container: {
    marginTop: 40,
    paddingBottom: 34,
  },
  containerTitle: {
    ...TYPOGRAPHY.medium,
    color: COLORS.darkGray,
    marginBottom: 12,
    marginLeft: 8,
  },
  input: {
    marginBottom: 8,
  },
  button: {
    marginBottom: 24,
  },
  statusText: {
    ...TYPOGRAPHY.regular,
    marginBottom: 14,
    marginTop: -10,
    alignSelf: "center",
  },
});

export default styles;
