import React, { useEffect, useState } from "react";
import ScrollScreen from "containers/ScrollScreen";
import NewsDetailHeader from "components/NewsDetailHeader";
import {
  NavigationParams,
  NavigationScreenProp,
  NavigationState,
} from "react-navigation";
import { fetchComments } from "services/commentsApi";
import { Comments } from "interfaces/Comments";
import CommentsList from "./CommentsSection";

type NavigationProps = {
  navigation: NavigationScreenProp<NavigationState, NavigationParams>;
};

const DetailNews = ({ navigation }: NavigationProps) => {
  const item = navigation.getParam("item");
  const [comments, setComments] = useState<Comments[]>([]);

  useEffect(() => {
    const setData = async () => {
      const dataToSet = await fetchComments(item.id);
      if (dataToSet != undefined) {
        setComments(dataToSet);
      }
    };
    setData();
  }, []);

  return (
    <ScrollScreen
      style={{ paddingTop: 24, paddingHorizontal: 16 }}
      extraScrollHeight={120}
    >
      <NewsDetailHeader item={item} />
      {comments && <CommentsList itemID={item.id} data={comments} />}
    </ScrollScreen>
  );
};

DetailNews.sharedElements = (route: { getParam: (arg0: string) => any }) => {
  const item = route.getParam("item");
  return [{ id: `item.${item.id}`, resize: "stretch", animation: "fade" }];
};

export default DetailNews;
