import React from "react";
import { Text, ActivityIndicator } from "react-native";
import Screen from "containers/Screen";

type LoadingProps = {
  isInternet: boolean | undefined;
};

const Loading = ({ isInternet }: LoadingProps) => {
  return (
    <Screen style={{ justifyContent: "center", alignItems: "center" }}>
      {isInternet ? <ActivityIndicator /> : <Text>No internet connection</Text>}
    </Screen>
  );
};

export default Loading;
