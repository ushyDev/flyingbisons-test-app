import React from "react";
import { FlatList, ActivityIndicator } from "react-native";
import AlbumItem from "components/AlbumItem/index";
import { Albums } from "interfaces/Albums";

type NewsListProps = {
  data: Albums[] | any;
  loadedAlbums: boolean;
};

const AlbumsList = ({ data, loadedAlbums }: NewsListProps) => {
  return (
    <>
      {!loadedAlbums ? (
        <ActivityIndicator />
      ) : (
        <FlatList
          numColumns={2}
          keyExtractor={(item) => item.id.toString()}
          data={data}
          renderItem={({ item }) => <AlbumItem item={item} />}
          columnWrapperStyle={{
            flex: 1,
            justifyContent: "space-around",
          }}
          style={{
            paddingTop: 24,
            paddingHorizontal: 16,
          }}
          initialNumToRender={20}
        />
      )}
    </>
  );
};

export default AlbumsList;
