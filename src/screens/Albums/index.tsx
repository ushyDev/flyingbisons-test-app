import React, { useContext } from "react";
import Screen from "containers/Screen";
import AppContext from "../../context/AppContext";
import AlbumsList from "./AlbumsList";

type AlbumProps = {};

const Album = (props: AlbumProps) => {
  const appContext = useContext(AppContext);
  const { albums, loadedAlbums } = appContext;
  return (
    <Screen>
      <AlbumsList data={albums} loadedAlbums={loadedAlbums} />
    </Screen>
  );
};

export default Album;
