import React, { useContext } from "react";
import Screen from "containers/Screen";
import NewsList from "./NewsList";
import AppContext from "../../context/AppContext";

type NewsProps = {};

const News = (props: NewsProps) => {
  const appContext = useContext(AppContext);
  const { posts, loadedPosts } = appContext;

  return (
    <Screen>
      <NewsList loadedPosts={loadedPosts} data={posts} />
    </Screen>
  );
};

export default News;
