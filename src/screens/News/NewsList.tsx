import React, { useMemo } from "react";
import { FlatList, ActivityIndicator } from "react-native";
import NewsItem from "components/NewsItem";
import { Posts } from "interfaces/Posts";

type NewsListProps = {
  data: Posts[] | any;
  loadedPosts: boolean;
};

const NewsList = ({ data, loadedPosts }: NewsListProps) => {
  const renderItem = ({ item }: { item: Posts }) => <NewsItem item={item} />;
  const memoizedItem = useMemo(() => renderItem, [data]);

  return (
    <>
      {!loadedPosts ? (
        <ActivityIndicator />
      ) : (
        <FlatList
          style={{ paddingHorizontal: 16, paddingTop: 24 }}
          renderItem={memoizedItem}
          data={data}
          keyExtractor={(item) => item.id.toString()}
          initialNumToRender={10}
        />
      )}
    </>
  );
};

export default NewsList;
