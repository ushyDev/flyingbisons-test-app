export default (state: any, action: any) => {
  switch (action.type) {
    case "SAVE_POSTS":
      return {
        ...state,
        posts: action.payload,
      };
    case "SAVE_ALBUMS":
      return {
        ...state,
        albums: action.payload,
      };
    case "SET_LOADED_POSTS":
      return {
        ...state,
        loadedPosts: action.payload,
      };
    case "SET_LOADED_ALBUMS":
      return {
        ...state,
        loadedAlbums: action.payload,
      };
    default:
      return state;
  }
};
