import React from "react";
import { Posts } from "interfaces/Posts";
import { Albums } from "interfaces/Albums";

type InitialStateType = {
  posts: Posts[];
  albums: Albums[];
  loadedPosts: boolean;
  loadedAlbums: boolean;
};

const initialState = {
  posts: [],
  albums: [],
  loadedPosts: false,
  loadedAlbums: false,
};

const AppContext = React.createContext<InitialStateType>(initialState);

export default AppContext;
