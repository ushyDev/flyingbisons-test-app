import React, { useReducer, useEffect } from "react";
import AppReducer from "./AppReducer";
import { fetchPosts } from "services/newsApi";
import { fetchAlbums } from "services/AlbumsApi";
import AppContext from "./AppContext";

const AppProvider = (props: any) => {
  const initialState = {
    posts: [],
    albums: [],
    loadedPosts: false,
    loadedAlbums: false,
  };

  const [state, dispatch] = useReducer(AppReducer, initialState);

  useEffect(() => {
    setPostsData();
    setAlbumsData();
  }, []);

  const setPostsData = async () => {
    const dataToSet = await fetchPosts();
    if (dataToSet != undefined) {
      dispatch({
        type: "SAVE_POSTS",
        payload: dataToSet,
      });
      dispatch({
        type: "SET_LOADED_POSTS",
        payload: true,
      });
    }
  };

  const setAlbumsData = async () => {
    const dataToSet = await fetchAlbums();
    if (dataToSet != undefined) {
      dispatch({
        type: "SAVE_ALBUMS",
        payload: dataToSet,
      });
      dispatch({
        type: "SET_LOADED_ALBUMS",
        payload: true,
      });
    }
  };

  return (
    <AppContext.Provider
      value={{
        posts: state.posts,
        albums: state.albums,
        loadedPosts: state.loadedPosts,
        loadedAlbums: state.loadedAlbums,
      }}
    >
      {props.children}
    </AppContext.Provider>
  );
};

export default AppProvider;
